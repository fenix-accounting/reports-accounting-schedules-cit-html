const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require("path");
var webpack = require("webpack");

module.exports = {
    entry: { index: path.resolve(__dirname, "src", "index.js") },
    output: {
      path: path.resolve(__dirname, 'dist')
    },
    devServer: {
      inline:true,
      port: 8000,
    },
    module: {
        rules: [
          {
            test: /\.(scss|css)$/,
            use: ["style-loader", "css-loader", "sass-loader"]
          },
          {
            test: /\.js$/,
            exclude: /node_modules/,
            use: ["babel-loader"]
          }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
          template: path.resolve(__dirname, ".", "index.html")
        }),
        new CopyWebpackPlugin({
          patterns: [
              { from: 'pages', to:'pages' }
          ]
        }), 
        new webpack.ProvidePlugin({
          jQuery: 'jquery',
          $: 'jquery',
          jquery: 'jquery',
          'window.jQuery': 'jquery',
      })
      ]
};