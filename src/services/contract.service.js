import { FiscalPeriod } from "@dealerfenix/core";

function contractRestService(restClient, $registry) {

    let getScheduleService = function() {
        return $registry.fetchService('accounting-schedules-api');
    }

    this.fetchSetup = function(locationId) {
        let citUrl = getScheduleService() + "/contracts/setup/" + locationId;

        return restClient.get(citUrl);
    }

    this.updateSetup = function(setup, linkedLocationIds) {
        let citUrl = getScheduleService() + "/contracts/setup";

        if (Array.isArray(linkedLocationIds)) {
            citUrl += "?" + restClient.buildParamString("locationId", linkedLocationIds);
        }

        return restClient.post(citUrl, setup);
    }


    this.fetchContracts = function(searchDto, format) {
        let citUrl = getScheduleService() + "/contracts";

        if (format) {
            citUrl += "?format=" + format;
        }

        return restClient.post(citUrl, searchDto);
    }


    this.fetchContract = function(contractId) {
        let citUrl = getScheduleService() + "/contracts/contract/" + contractId;

        return restClient.get(citUrl);
    }

    this.updateContract = function(contract) {
        let citUrl = getScheduleService() + "/contracts/contract";

        return restClient.post(citUrl, contract);
    }

    this.searchByContractsInTransitByControlNumber = function(locationId, controlNumber) {
        return restClient.get(getScheduleService() + "/contracts/" + locationId + "?controlNumber=" + controlNumber);
    }

    this.fetchAverageDays = (locationId, months)=> {
        const period = FiscalPeriod.now();

        const kpis = [];
        
        for (let i=0; i < months; i++) {
            kpis.push({requestId:period.minusMonths(i).asInt(), locationId:locationId, kpi:"/VARIABLE_RETAIL/CIT_DAYS", period:period.minusMonths(i)});
        }

        const url = $registry.fetchService("kpi-api") + "/kpis";

        return restClient.post(url, kpis);
    }

}

contractRestService.$inject = ["restClient", "$registry"];

export default contractRestService;