import {Observable} from '@dealerfenix/ngreports';

function citAppService($sessionStorage) {
    
    let observable = new Observable();

    this.getSearchDto = function() {
        if (!this.searchDto) {
            this.searchDto = {
                statuses:[],
                statuses2:[],
                financeManagerIds:[],
                locationIds:[]
            }
        } else {
            if (!this.searchDto.locationIds) {
                this.searchDto.locationIds = [];
            }
        }

        return this.searchDto;
    }

    this.setSearchDto = function(searchDto) {
        this.searchDto = searchDto;
    }

    this.notify = function(event) {
        observable.notify(event)
    }

    this.addListener = function(listener) {
        observable.addListener(listener);
    }

    this.removeListener = function(listener) {
        observable.removeListener(listener);
    }

    this.setManagers = function(managers) {
        this.managers = managers;
        observable.notify({event:'fieldChange', field:'managers'})
    }

    this.getManagers = function() {
        return this.managers;
    }

    this.showContract = function(contract, searchText) {
        if (!contract)
			return true;

		if (this.searchDto.statuses && this.searchDto.statuses.length > 0) {
			let foundIt = false;

			for (let i=0; i < this.searchDto.statuses.length; i++) {
				if (this.searchDto.statuses[i] == contract.citStatus || (this.searchDto.statuses[i]  == '' && contract.citStatus == null) ) {
					foundIt = true;
					break;
				}
			}

			if (!foundIt)
				return false;
		
		}

        // check status2
        if (this.searchDto.statuses2 && this.searchDto.statuses2.length > 0) {
			let foundIt = false;
            console.log("checking status2 " + this.searchDto.statuses2);

			for (let i=0; i < this.searchDto.statuses2.length; i++) {
				if (this.searchDto.statuses2[i] == contract.citStatus2 || (this.searchDto.statuses2[i]  == '' && contract.citStatus2 == null) ) {
					foundIt = true;
					break;
				}
			}

			if (!foundIt)
				return false;
		
		}

		if (searchText) {
			let foundIt = false;

			let keys = ["controlNumber", "dealNumber", "stockNumber", "controlDescription", "lenderName"];

			for (let i=0; i < keys.length; i++) {
				const key = keys[i];
				if (contract[key] && contract[key].startsWith(searchText))
				return true;
			}


			if (!foundIt)
				return false;
		}

		return true;
    }
}

citAppService.$inject = ["$sessionStorage"]

export default citAppService;