import 'angular';
import 'angular-route';
import 'ngstorage';
import 'jquery';
import 'bootstrap/dist/js/bootstrap.js';
import 'angular-ui-bootstrap';

import '@dealerfenix/fenixcore';
import '@dealerfenix/ngreports';
import '@dealerfenix/accounting-core'
import '@dealerfenix/sales-core';
import '@dealerfenix/schedules-core'

import routes from "./routes/app.routes";
import dealStatusCardComponent from './components/deal-status-card.component';
import filterToolbar from './components/filters-toolbar.component';
import citFiltersComponent from './components/filters.component';

import detailController from "./controllers/detail.controller";
import mainController from "./controllers/main.controller";
import loaderController from './controllers/loader.controller';
import logWorkSheetController from './controllers/worksheet.controller';
import contractRestService from './services/contract.service';

import '../style/style.scss';
import citAppService from './services/citapp.service';

// main declaration
angular.module('citApp', 
	['fenixCore', 'ngReports', 'schedulesCoreApp', "salesCore", 'ngWindows','ngStorage', 'ui.bootstrap'])
    .config(["$routeProvider", "$httpProvider", routes])
	.controller('detailCntrl', detailController)
	.controller('mainCntrl', mainController)
	.controller('loaderController', loaderController)
	.controller('logWorkSheetController', logWorkSheetController)
	.component('citStatus', dealStatusCardComponent)
	.component("citFilters", citFiltersComponent)
	.component("citToolbar", filterToolbar)
	.service('$contracts', contractRestService)
	.service("citAppService", citAppService)
	.run(["scheduleInit", "$location", "$sessionStorage", function(scheduleInit, $location, $sessionStorage){
		scheduleInit.init(process.env.NODE_ENV === 'development')
		if (!$sessionStorage.user) {
			if ($location.path()!="" && $location.path() != "/") {
				let path = $location.absUrl().replace($location.path(), "");

				let argsIndex = path.indexOf("?");
				if ( argsIndex > -1) {
					path = path.substring(0, argsIndex);
				}

				window.location.href = path + "?redirectUrl=" + btoa($location.url());
			}
		}
	}]);