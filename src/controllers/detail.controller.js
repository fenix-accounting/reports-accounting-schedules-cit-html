import {DLG_YES} from '@dealerfenix/ngwindows';
import { RebuildDialog } from '@dealerfenix/schedules-core';

function detailController($scope, $location, userService, scheduleService, $sort, windowService, dialogService, $printer, salesRestService, $contracts, id) {

	$scope.total = 0;
    
    let updateStats = function() {
    
        $scope.total = 0;

        if ($scope.detail.openingBalance) {
            $scope.total += $scope.calcOpenBalance();
        }

        for (let i=0; i < $scope.detail.reportDetail.length; i++) {
            let record = $scope.detail.reportDetail[i];
            
            $scope.total += record.amount;
        }
    }

    $scope.calcOpenBalance = function() {
        if (!$scope.detail)
            return 0;

        let openBalance = $scope.detail.openingBalance;

        if (!openBalance)
            return 0;

        return openBalance.current + openBalance.overThirty + openBalance.overSixty + openBalance.overNinety + openBalance.overOneHundredTwenty;
    }

    let fetchDealInfo = function() {
        salesRestService.fetchDeal($scope.cit.dealId).then(function(deal) {
            $scope.deal = deal;
        }, function(error) {
            console.log(error);
        });
    }

    let fetchComments = function() {
        scheduleService.fetchContractComments(id).then(comments=>{
            $scope.comments = comments;
        }, error=>{
            dialogService.showError(error);
        })
    }

    let fetchCitInfo = function() {
        $contracts.fetchContract(id).then(contract=>{
            $scope.cit = contract;

            if ($scope.cit.dealId) {
                fetchDealInfo();
            }

            fetchComments();

            fetchScheduleDetail(null);
        })
    }

    let fetchScheduleDetail = function(format) {
        windowService.aSyncRun(scheduleService.fetchScheduleControlDetail($scope.cit.scheduleId, $scope.cit.controlNumber, format), detail=>{
            $scope.detail=detail;
            $sort.setObjects($scope.detail.reportDetail);
            updateStats();
        }, err=>{
            console.log("error :" + err);
        });
    }

    $scope.onBack = function() {
        $location.path("/");
    }

    let onExport = function(format) {
        windowService.aSyncRun(scheduleService.fetchScheduleControlDetail(scheduleId, control, format), response=>{
            $printer.print(response);
        }, err=>{
            console.log("error :" + err);
        });
    }

    setTimeout(()=>{
        fetchCitInfo();
    }, 300);

    
    $scope.onComment = function(comment) {
		scheduleService.postContractComment([{objectId:id, body:comment, postedBy:userService.getUser().username, postedOn:new Date()}]).then(()=>{
			windowService.toast("Comment Added");
            fetchComments();
		}).catch(err=>{
			console.log("error posting comment "+ err);
			dialogService.open({template:"ERROR", message:"Failed to add comment"});
		})
	}

    $scope.onContractChange = function() {
        $contracts.updateContract($scope.cit).then(()=>{
			windowService.toast("Contract Update");
            fetchCitInfo();
		}).catch(err=>{
			console.log("error posting comment "+ err);
			dialogService.open({template:"ERROR", message:"Failed to add comment"});
		})
    }

    $scope.isAdmin = function() {
        return userService.getUser().role == "ADMIN";
    }

    $scope.onPatchBalance = function(balance) {
        dialogService.open({template:"YES_NO", message:"Are you sure you want to patch the beginning balance"}, result=>{

            if (DLG_YES == result) {
                scheduleService.patchBeginBalance($scope.cit.scheduleId, [{controlNumber:$scope.cit.controlNumber, scheduleBalance:{current: balance.toFixed(2)}}]);
            }

        })
    }

    $scope.onRebuild = ()=> {
        dialogService.openDialog({controlNumber:$scope.cit.controlNumber, scheduleId:$scope.cit.scheduleId}, RebuildDialog, ()=>{
            fetchCitInfo();
        })
    }

    $scope.menuOptions = {items:[
        {link:onExport, icon:"far fa-file-pdf fa-lg", data:"PDF"},
        {link:onExport, icon:"far fa-file-excel fa-lg", data:"XLS"},
        // {link:refreshData, icon:"fas fa-sync fa-lg"}
    ]};

}

detailController.$inject = ["$scope", "$location", "userService", "scheduleService", "$sort", "windowService", "dialogService",  "$printer", "salesRestService", "$contracts", "id"];

export default detailController;