import {VehicleDialog} from "@dealerfenix/schedules-core";
import {ReportsMenuBuilder} from '@dealerfenix/ngreports';
import {RestError, StoreSelectorDialog} from '@dealerfenix/fenixcore';
import statusDialog from "../dialogs/status.dialog";
import citSetupDialog from "../dialogs/setup.dialog";

function mainController($scope, citAppService, $sessionStorage, $contracts, scheduleService, $sort, userService, windowService, dialogService, $printer, $location) {

	$scope.searchDto = citAppService.getSearchDto(); 
	if (!$sort.getOrderBy()) {
		$sort.setOrderBy("age", false);
	}
	let calcTotals = function() {
		$scope.totalBalance = 0;
		$scope.agedBalance  = 0;
		$scope.agedContracts = 0;

		if (!$scope.contracts) {
			return;
		}

		$scope.totalContracts = $scope.contracts.length;
		let managers = [];

		for (let i=0; i < $scope.contracts.length; i++) {
			let contract = $scope.contracts[i];

			if ($scope.showContract(contract)) {
				$scope.totalBalance += contract.balance;

				if ($scope.isAged(contract)) {
					$scope.agedBalance += contract.balance;
					$scope.agedContracts++;
				}
	
				if (contract.fiMgrName){
					if (managers.indexOf(contract.fiMgrName) == -1) {
						managers.push(contract.fiMgrName);
					}	
				}
			}
		}

		citAppService.setManagers(managers);
		$scope.daysToFund = 0;

		if ($scope.citSetup && $scope.citSetup.showDaysToTrendMonths) {
			$contracts.fetchAverageDays(userService.getCurrentLocation(), $scope.citSetup.showDaysToTrendMonths).then(kpiDaysResult=>{
				let days = 0;
				let months = 0;

				for (let i=0; i < $scope.citSetup.showDaysToTrendMonths; i++) {
					if (kpiDaysResult[i].value > 0) {
						days += kpiDaysResult[i].value ;
						months++;
					}
				}

				console.log("days is " + days);
				$scope.daysToFund = days / months;
			}, err=>{
				console.log("error is " + err);
			});
		}

	}

	let resultsHandler = function(results) {
		if (!$sort.getOrderBy()) {
			$sort.setOrderBy("age", false);
		}

		$scope.contracts = results.contractsInTransit;
		$scope.lastRefreshed = results.lastRefreshed;
		$scope.schedules = results.schedules; 

		$sort.setObjects($scope.contracts);

		windowService.sizeWidget("tblData");
		calcTotals();
	}

	let fetchContracts = function(handler, format) {

		$sessionStorage.citSearchDto = citAppService.getSearchDto();

		const _searchDto = angular.copy($scope.searchDto);

		if (!Array.isArray($scope.searchDto.locationIds) || $scope.searchDto.locationIds.length == 0) {
			_searchDto.locationIds = [userService.getCurrentLocation()];
		}

		_searchDto.orderBy = $sort.getOrderBy();
		_searchDto.sortAscending = $sort.getSortAscending();

		if (userService.getCurrentLocation() == "1028-06") {
			_searchDto.includePipeline = true;
		}

		$scope.showLocationId = _searchDto.locationIds.length > 1;

		windowService.aSyncRun($contracts.fetchContracts(_searchDto, format), contracts=>{
			handler(contracts);
		}, error=>{
			console.log(JSON.stringify(error));
			dialogService.showError("Error fetching contracts");
		});
	}

	$scope.showDaysToFund = ()=> {
		return $scope.citSetup && $scope.citSetup.showDaysToTrendMonths  > 0;
	}

	$scope.isAged = function(contract) {
		return contract.age > $scope.agedDays;
	}

	$scope.onFilter = function(field) {
		console.log("onFilter " + field);
		if (field != 'status') {
			$scope.refresh();
		} else {
			calcTotals();
		}
	}

	let checkisTablet = function() {
        $scope.isTabletMode = windowService.windowSize().width < 1440;
	}


	window.addEventListener('resize', ()=>{
        checkisTablet();
        $scope.$apply();
    });

	$scope.onComment = function(id, comment, contract) {
		let newComment = {objectId:id, body:comment, postedBy:userService.getUser().username, postedOn:new Date()};

		if (!id) {
			newComment.objectId = contract.dealId;
			newComment.topic = "DEAL_CIT";
		}

		scheduleService.postContractComment([newComment]).then(()=>{
			windowService.toast("Comment Added");
			$scope.refresh();
		}).catch(err=>{
			console.log("error posting comment "+ err);
			dialogService.open({template:"ERROR", message:"Failed to add comment"});
		})
	}


	$scope.onDeal = function(contract) {
		if (contract.dealId) {		
			$location.path("/deal/" + contract.dealId);
		} else {
			alert("deal Id not found");
		}

	}

	$scope.onVehicle = function(contract) {
		if (contract.vehicleId) {
			dialogService.openDialog({vehicleId:contract.vehicleId}, VehicleDialog);
			
		} else {
			alert("vehicle Id not found");
		}
	}
	
	$scope.onViewAll = function(objectId, contract) {
		if (!objectId) {
			objectId = contract.dealId;
		}
		
		scheduleService.fetchContractComments(objectId).then(comments=>{
			let dialogParams =  {templateURL:"commentListModal.html", size:'lg', controller:'commentListCntrl2'};

			dialogService.openDialog({comments:comments}, dialogParams);
		}, error=>{
			console.log("error fetching comments:" + error);
			dialogService.open({template:"ERROR", message:"Error fetching comments"});
		});
	}

	$scope.showContract = function(contract) {
		return citAppService.showContract(contract, $scope.searchText);
	}

	$scope.onDetail = function(record) {
		$location.path("/detail/"+ record.id);
	}

	$scope.onSearch = function(control) {
		console.log("in on search " + control);
		$contracts.searchByContractsInTransitByControlNumber(userService.getCurrentLocation(), control).then(results=>{
			let contracts = results.contractsInTransit;
			console.log("contracts len is " + contracts.length); 
			if (contracts.length == 1) {
				$location.path("/detail/"+ contracts[0].id);
			 }
			// 
		}, error=>{
			console.log("error " + error);
		})

	}

	$scope.refresh = function() {
		fetchContracts(resultsHandler);
	}

	let updateStatus = function(contract, statuses, callBack) {
        dialogService.openDialog({statuses:statuses}, statusDialog, result=>{
			callBack(result.status);

            //contract.citStatus = result.status;

			$contracts.updateContract(contract).then(()=>{
				windowService.toast("Contract Update");
				calcTotals();
			}).catch(err=>{
				console.log("error posting comment "+ err);
				dialogService.open({template:"ERROR", message:"Failed to add comment"});
			})
        });
    }

	$scope.onUpdateStatus = function(contract) {
		updateStatus(contract, $scope.citSetup.statuses, status=>{
			contract.citStatus = status
		});
    }

	$scope.onUpdateStatus2 = function(contract) {
		updateStatus(contract, $scope.citSetup.statuses2, status=>{
			contract.citStatus2 = status
		});
    }

	let doExport = function(format) {

		let printHandler = function(printFile) {
			$printer.print(printFile);
		}

		fetchContracts(printHandler, format);
	}

	let onRefresh = function() {
        if (!$scope.refreshing) {
            $scope.refreshing = true;

			let ids = [];

			for (let i=0; i < $scope.schedules.length; i++) {
				ids.push($scope.schedules[i].scheduleId);
			}

			console.log("need to refresh " + JSON.stringify(ids));

            windowService.aSyncRun(scheduleService.refreshSchedule(ids), ()=>{
                windowService.toast("Schedule Refreshed");
                $scope.refreshing = false;

				setTimeout(()=>{
					$scope.refresh();
				}, 300);
            }, err=>{
                console.log("error refreshing schedule "+ JSON.stringify(err));
                $scope.refreshing = false;
                dialogService.open({template:"ERROR", message:"Failed to refresh schedule"});
            })
        }
    }

	$scope.menuOptions = ReportsMenuBuilder(doExport);

	let onSetup = function() {
		let doSave = (setup, locationIds)=> {
			$contracts.updateSetup(setup, locationIds).then(()=>{
				fetchSetup(true);
			}, err=>{
				console.log("an error occurred saving the cit setup " + err);
			})
		}

		let doCopy = (setup)=> {
            dialogService.openDialog({}, StoreSelectorDialog, locationIds=>{
				doSave(setup, locationIds);
            });
        }

		dialogService.openDialog({}, citSetupDialog, (result)=>{

			if (result.action == "COPY") {
				doCopy(result.setup);
			}else {
				doSave(result.setup, null);
			}
			
		})
	}

	if (userService.getPrimaryRole() == "ADMIN"){
		$scope.menuOptions.items.push({icon:"fas fa-cog fa-lg", link:onSetup});
	}

	$scope.menuOptions.items.push({icon:"fas fa-sync fa-lg", link:onRefresh}); //TODO: disable for CDK

	let fetchSetup = function(force) {
		const key = "citSetup." + userService.getCurrentLocation();

		const citSetup = userService.getFromLocal(key);

		let defaultTrue = (setup, field)=>{
			if (setup.showFields[field] === undefined) {
				setup.showFields[field] = true;
			}
		}

		if (!force && citSetup) {
			$scope.citSetup = citSetup;
			$scope.agedDays = $scope.citSetup.agedDays;


			defaultTrue(citSetup, "controlNumber");
			defaultTrue(citSetup, "floorPlan");

			calcTotals();
		} else {
			console.log("fetching cit setup from database ");
			$contracts.fetchSetup(userService.getCurrentLocation()).then( setup=> {

				if (!setup.showFields) {
					setup.showFields = {};
				}

				defaultTrue(setup, "controlNumber");
				defaultTrue(setup, "floorPlan");

				$scope.citSetup = setup;
				$scope.agedDays = $scope.citSetup.agedDays;
	
				userService.storeInLocal(key, setup,  1000*60*5); // cache for 5 minutes
				calcTotals();
			}, error=>{
	
			});
		}
	
	}

	const endpointId = userService.getEndPointId();

	$scope.formatLocationId = (locationId) => {
		return locationId.substring(endpointId.length+1);
	}
	
	$scope.showField = (field)=>{
		if ($scope.citSetup && $scope.citSetup.showFields && $scope.citSetup.showFields[field]) {
			return true;
		}

		return false;
	}
	$scope.getStatusColor = (contract)=> {
		if ($scope.isAged(contract))
			return 'bg-red';

		if (!contract.id)
			return 'bg-blue';

		return "";
	}

	setTimeout(()=>{
		checkisTablet();
		fetchSetup();

		let eventLister= function(event) {
			if (event.event == "filterChange") {
				$scope.onFilter(event.field);
			}
		}

		citAppService.addListener(eventLister);

		
		$('#tblData').on('scroll', function () {
			$('#tblHeader').scrollLeft($(this).scrollLeft());
		});
	
		$scope.refresh();
	}, 300);
}

mainController.$inject = ["$scope", "citAppService", "$sessionStorage", "$contracts", "scheduleService", "$sort", "userService", "windowService", "dialogService", "$printer", "$location"];

export default mainController;