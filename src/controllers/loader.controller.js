
function loaderController($scope, $location) {

    $scope.onSuccess=function(){
        $location.path("/cit");
    }

}

loaderController.$inject = ["$scope", "$location"];

export default loaderController;