function logWorkSheetController($scope, $location, salesRestService, id) {

    salesRestService.fetchDeal(id).then(function(deal) {
        $scope.deal = deal;
    }, function(error) {
        console.log(error);
    });

    $scope.onCancel = function() {
        $location.path("/");
    }
}

logWorkSheetController.$inject=["$scope", "$location", "salesRestService", "id"];

export default logWorkSheetController;