// setup route table
function routes($routeProvider, $httpProvider) {
	$routeProvider

	// route for the home page
	.when('/', {
		templateUrl : './pages/loader.html',
		controller  : 'loaderController',
	})

	.when('/cit', {
		templateUrl : './pages/main.html',
		controller  : 'mainCntrl',
	})

	.when("/deal/:id", {
		templateUrl : './pages/worksheet.html',
		controller : 'logWorkSheetController',
		resolve: {
			id : ["$route", function($route) {
				return $route.current.params.id;
			}]
		}
	})
	
	.when("/detail/:id", {
		templateUrl : './pages/detail.html',
		controller  : 'detailCntrl',
		resolve: {
			id : ["$route", function($route) {
				return $route.current.params.id;
			}],
		}
	})

	.when("/detail/:schedule/:control", {
		templateUrl : './pages/detail.html',
		controller  : 'detailCntrl',
		resolve: {
			controlNumber : ["$route", function($route) {
				return $route.current.params.control;
			}],
			scheduleId : ["$route", function($route) {
				return $route.current.params.schedule;
			}]
		}
	})

	.otherwise({
		redirectTo : '/'
	});


    $httpProvider.interceptors.push('httpUnauthorized');	
}

export default routes;
