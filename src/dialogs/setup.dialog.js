let citSetupDialog = {
    template:
    `<div class="modal-header">
        <h5 class="df-bold">CIT Setup</h5>
    </div>
    <div class="modal-body citSetup" id="modal-body">
    
        <div class="setupRow">
            <span>Aged Days</span>

            <input class="form-control width3" ng-model="$ctrl.setup.agedDays">
        </div>

        <div class="setupRow">    
            <div class="checkbox"><label><input ng-model="$ctrl.setup.doTrackStatus" type="checkbox">Track Funding</label></div>

            Show Days To Fund Trend (Months)
            <input class="form-control width4" ng-model="$ctrl.setup.showDaysToTrendMonths">
        </div>


        <div class="setupRow">
            <label>Status List</label>

            <div class="btn-group">
                <input class="form-control width12" ng-model="$ctrl.statusText">
            </div>

            <div class="btn-group">
                <button class="btn btn-default" ng-click="$ctrl.onAddStatus()">Add</button>
            </div>

            <div style="margin-top:20px">
                Select the checkbox if the status should mark the contract as funded.
                <table class="table borderless">
                    <tr ng-repeat="status in $ctrl.setup.statuses track by $index">
                        <td style="width:25px"><input ng-checked="$ctrl.isFundedStatus(status)"  ng-click="$ctrl.toggleFundedStatus(status)" type="checkbox"></td>
                        <td>{{status}}</td>
                        <td style="width:25px"><a href ng-click="$ctrl.onDelete($index)"><i class="fas fa-times"></i></a></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="setupRow">    
            <div class="checkbox"><label><input ng-model="$ctrl.setup.doStatus2" type="checkbox">Add Secondary Status</label></div>
        </div>

        <div class="setupRow">
            <label>Secondary Status</label>

            <div class="btn-group">
                <input class="form-control width12" ng-model="$ctrl.status2Text">
            </div>

            <div class="btn-group">
                <button class="btn btn-default" ng-click="$ctrl.onAddStatus2()">Add</button>
            </div>

            <div style="margin-top:10px">
                <table class="table borderless">
                    <tr ng-repeat="status in $ctrl.setup.statuses2 track by $index">
                        <td style="width:25px"></td>
                        <td>{{status}}</td>
                        <td style="width:25px"><a href ng-click="$ctrl.onDeleteStatus2($index)"><i class="fas fa-times"></i></a></td>
                    </tr>
                </table>
 
            </div>
        </div>

        <div class="setupRow">
            <label>Show Fields</label>
            <div class="checkbox"><label><input type="checkbox" ng-model="$ctrl.setup.showFields['controlNumber']"> Control #</label></div>
            <div class="checkbox"><label><input type="checkbox" ng-model="$ctrl.setup.showFields['floorPlan']"> Floor Plan</label></div>
            <div class="checkbox"><label><input type="checkbox" ng-model="$ctrl.setup.showFields['salesPersonName']"> Sales Person Name</label></div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="pull-left">
            <a href ng-click="$ctrl.onCopy()">Copy To</a>
        </div>

        <div class="pull-right">
            <button class="btn btn-primary" type="button" ng-click="$ctrl.onOk()">Ok</button>
            <button class="btn btn-default" type="button" ng-click="$ctrl.onCancel()">Close</button>
        </div>
    </div>`,
    controller:setupController
}

function setupController($uibModalInstance, $contracts, userService,  data) {

    const self = this;

    let defaultTrue = (setup, field)=>{
        if (setup.showFields[field] === undefined) {
            setup.showFields[field] = true;
        }
    }

    this.copyAll = false;

    let fetchSetup = function() {
        $contracts.fetchSetup(userService.getCurrentLocation()).then(setup=>{
            self.setup=setup;

            if (!self.setup.showFields) {
                self.setup.showFields = {}
            }

            defaultTrue(self.setup, "controlNumber");
			defaultTrue(self.setup, "floorPlan");
        }).catch(err=>console.log("error fetching setup:" + err));
    }

    this.isFundedStatus = (status)=> {
        if (Array.isArray(self.setup.fundedStatuses)) {
            return self.setup.fundedStatuses.indexOf(status) > -1;
        }

        return false;
    }

    this.toggleFundedStatus = (status)=> {
        if (!Array.isArray(self.setup.fundedStatuses)) {
            self.setup.fundedStatuses = [];
        }
            
        const index = self.setup.fundedStatuses.indexOf(status);

        if (index == -1) {
            self.setup.fundedStatuses.push(status);
        } else {
            self.setup.fundedStatuses.splice(index, 1);
        }
    }

    fetchSetup();

    this.onDelete = function($index) {
        self.setup.statuses.splice($index, 1);

        self.setup.statuses.sort();
    }

    this.onAddStatus = function() {
        if (!Array.isArray(self.setup.statuses)) {
            self.setup.statuses = [];
        }

        self.setup.statuses.push(self.statusText);
        self.setup.statuses.sort();

        self.statusText = "";
    }

    this.onAddStatus2 = function() {
        if (!Array.isArray(self.setup.statuses2)) {
            self.setup.statuses2 = [];
        }

        self.setup.statuses2.push(self.status2Text);
        self.setup.statuses2.sort();

        self.status2Text = "";
    }

    this.onDeleteStatus2 = function($index) {
        self.setup.statuses2.splice($index, 1);

        self.setup.statuses2.sort();
    }


    this.onCopy = ()=> {
        $uibModalInstance.close({setup:self.setup, action:"COPY"});
    }

    this.onOk = function() {
        $uibModalInstance.close({setup:self.setup, action:"UPDATE"});
    }

    this.onCancel = function() {
        $uibModalInstance.dismiss('cancel');
    }
}

setupController.$inject = ["$uibModalInstance", "$contracts", "userService", "data"];

export default citSetupDialog;