let citFilterDialog = {
    template:
    `<div class="modal-header">
        Filters
    </div>
    <div class="modal-body" id="modal-body">
        <cit-filters cit-setup="$ctrl.citSetup"></cit-filters>
    </div>
    <div class="modal-footer">
        <div class="pull-right">
            <button class="btn btn-default" type="button" ng-click="$ctrl.onCancel()">Close</button>
        </div>
    </div>`,
    size:'sm',
    controller:filterDialogController
}

function filterDialogController($uibModalInstance,  data) {

    this.citSetup = data.citSetup;

    this.onOk = function() {
        $uibModalInstance.close();
    }

    this.onCancel = function() {
        $uibModalInstance.dismiss('cancel');
    }

}

filterDialogController.$inject = ["$uibModalInstance", "data"];

export default citFilterDialog;