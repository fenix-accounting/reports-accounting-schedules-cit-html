import {DropDownOption} from '@dealerfenix/ngreports';

let citFiltersComponent = {
    bindings:{
		citSetup:'<'
    },
    template:
    `<div class="cit-toolbar">
        <div class="filter-group" ng-show="$ctrl.hasMultipleLocations()">
            <location-dropdown selected="searchDto.locationIds" on-change="$ctrl.onLocationChange(selected)"></location-dropdown>
        </div>

        <div class="filter-group">
            <drop-down options="$ctrl.managers" title="'Manager'" close="$ctrl.onManagerFilter(data)"></drop-down>
        </div>

        <div class="filter-group" ng-if="$ctrl.citSetup.doTrackStatus">
            <drop-down options="$ctrl.statuses" title="'Status'" close="$ctrl.onStatusFilter(data)"></drop-down>
        </div>

		<div class="filter-group" ng-if="$ctrl.citSetup.doStatus2">
			<drop-down options="$ctrl.statuses2" title="'Status 2'" close="$ctrl.onStatus2Filter(data)"></drop-down>
		</div>
    </div>`,
    controller:filterToolbarController
}


function filterToolbarController(citAppService, userService, windowService, dialogService) {

    let self = this;

	let eventHandler = function() {
		let managers = citAppService.getManagers();

		self.managers = [];
		if (managers) {
			for (let i=0; i < managers.length; i++) {
				let isManagerSelected = (!Array.isArray(self.searchDto.financeManagerIds) || self.searchDto.financeManagerIds.length == 0) || self.searchDto.financeManagerIds.indexOf(managers[i]);
				self.managers.push(new DropDownOption(managers[i], managers[i], isManagerSelected));	
			}
		}
	}

	this.$onInit = function() {
		citAppService.addListener(eventHandler);
		eventHandler();
	}

	this.$onDestroy = function() {
		citAppService.removeListener(eventHandler);
	}

    this.$onChanges = function() {
		self.searchDto = citAppService.getSearchDto();

		if (self.citSetup) {
			const statuses = self.citSetup.statuses;
			self.statuses = [];
			if (Array.isArray(statuses)){

				if (!Array.isArray(self.searchDto.statuses)) {
					self.searchDto.statuses = [];
				}

				self.statuses.push(new DropDownOption("", "",  self.searchDto.statuses.length == 0 || self.searchDto.statuses.indexOf("") > -1));

				for (let i=0; i < statuses.length; i++) {
					
					let isStatusSelected =  self.searchDto.statuses.length == 0 || self.searchDto.statuses.indexOf(statuses[i]) > -1;

					self.statuses.push(new DropDownOption(statuses[i], statuses[i], isStatusSelected));	
				}
			}

			// statuses 2
			const statuses2 = self.citSetup.statuses2;
			self.statuses2 = [];
			if (Array.isArray(statuses2)){

				if (!Array.isArray(self.searchDto.statuses2)) {
					self.searchDto.statuses2 = [];
				}

				self.statuses2.push(new DropDownOption("", "",  self.searchDto.statuses.length == 0 || self.searchDto.statuses.indexOf("") > -1));

				for (let i=0; i < statuses2.length; i++) {
					
					let isStatusSelected =  self.searchDto.statuses2.length == 0 || self.searchDto.statuses2.indexOf(statuses[i]) > -1;

					self.statuses2.push(new DropDownOption(statuses2[i], statuses2[i], isStatusSelected));	
				}
			}
		}
    }

    this.hasMultipleLocations = function() {
		return userService.getLocations().length > 1;
	}

    this.onManagerFilter = function(selection) {
		
		let selectedIds = [];
		for (let i=0; i < selection.length; i++) {
			if (selection[i].selected) {
				selectedIds.push(selection[i].value);
			}
		}
		self.searchDto.financeManagerIds = selectedIds;
		citAppService.notify({event:'filterChange', 'field':'manager'});
	
	}

    this.onLocationChange = function(locationIds) {

		if ( !(locationIds.length == 0 && $scope.searchDto.locationIds == 0) ) {
			self.searchDto.locationIds = locationIds;
			citAppService.notify({event:'filterChange', 'field':'location'});
		}
	
	}

	this.onStatusFilter = function(selection) {
		
		let statuses = [];

		for (let i=0; i < selection.length; i++) {
			let item = selection[i];
			if (item.selected) {
				statuses.push(item.value)
			}
		}

		self.searchDto.statuses = statuses;
		citAppService.notify({event:'filterChange', 'field':'status'});
	}

	this.onStatus2Filter = function(selection) {
		
		let statuses = [];

		for (let i=0; i < selection.length; i++) {
			let item = selection[i];
			if (item.selected) {
				statuses.push(item.value)
			}
		}

		console.log("Settnig statuses 2 to  " + JSON.stringify(statuses));

		self.searchDto.statuses2 = statuses;
		citAppService.notify({event:'filterChange', 'field':'status2'});
	}

}

filterToolbarController.$inject = ["citAppService", "userService", "windowService", "dialogService"];

export default citFiltersComponent;