import {ReportsMenuBuilder, DropDownOption} from '@dealerfenix/ngreports';
import citFilterDialog from '../dialogs/filters.dialog';

let filterToolbar = {
    bindings:{
		citSetup:'<',
        isTabletMode:'<',
        onChange:"&"
    },
    template:
    `<div class="fx-wrapper cit-toolbar-inline" ng-hide="$ctrl.isTabletMode">
		<cit-filters cit-setup="$ctrl.citSetup"></cit-filters>
    </div>
    <div class="fx-wrapper" ng-show="$ctrl.isTabletMode">
        <div style="margin:10px 12px">
            <a href ng-click="$ctrl.onFilterDialog()"><i class="fas fa-filter fa-lg"></i></a>
        </div>
    </div>`,
    controller:filterToolbarController
}


function filterToolbarController(citAppService, dialogService) {

    let self = this;

	this.onFilterDialog = function() {
		dialogService.openDialog({citSetup:self.citSetup}, citFilterDialog);
	}
}

filterToolbarController.$inject = ["citAppService", "dialogService"];

export default filterToolbar;