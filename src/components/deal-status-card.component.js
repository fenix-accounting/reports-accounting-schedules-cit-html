import statusDialog from "../dialogs/status.dialog";

let dealStatusCardComponent = {
    bindings:{
        contract:"=",
        onChange:"&"
    },
    template:
    `<div class="card-box">
	     <table style="table-layout: fixed; width:100%; padding:5px">
              <tr>
                  <td class="width8">Deal #</td>
                  <td>{{$ctrl.contract.dealNumber}}</td>
                  <td class="width8">CIT Status</td>
                  <td>
                    <a ng-show="$ctrl.contract.citStatus" href ng-click="$ctrl.onUpdateStatus()">{{$ctrl.contract.citStatus}}</a>
                    <a ng-hide="$ctrl.contract.citStatus" href ng-click="$ctrl.onUpdateStatus()"> - </a>
                  </td>
              </tr>

              <tr>
                  <td class="width8">Deal Status</td>
                  <td>
                      <span><label ng-class="$ctrl.contract.dealStatus == 'CLOSED_WON' ? 'label label-primary' : 'label label-default'">{{$ctrl.contract.dealStatus}}</label></span>
                  </td>
              </tr>
              
              <tr>
                  <td>Sale</td>
                  <td>{{$ctrl.contract.dealDate | date:"MM/dd/yy"}}</td>
              </tr>

              <tr>
                  <td>F&I Mgr</td>
                  <td>{{$ctrl.contract.fiMgrName}}</td>
	          </tr>
              
	      </table>
    </div>`,
    controller: dealCardController
}

function dealCardController(dialogService) {

    const self = this;

    this.onUpdateStatus = function() {
        dialogService.openDialog({}, statusDialog, result=>{
            self.contract.citStatus = result.status;
            self.onChange({});
        });
    }
}

dealCardController.$inject = ["dialogService"];

export default dealStatusCardComponent;